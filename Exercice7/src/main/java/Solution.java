import java.util.ArrayList;
import java.util.List;

public class Solution {
    private List<PackageOwn> packages;
    private int totalWeight = 0;
    private int totalValue = 0;

    public Solution() {
        this.packages = new ArrayList<>();
    }

    public Solution(Solution solution){
        this.totalWeight = solution.getTotalWeight();
        this.totalValue = solution.getTotalValue();

        this.packages = new ArrayList<>();
        for (PackageOwn packageOwn:solution.getPackages()) {
            this.packages.add(packageOwn);
        }
    }

    public List<PackageOwn> getPackages() {
        return packages;
    }

    public void addPackage(PackageOwn packageOwn){
        this.packages.add(packageOwn);
        this.totalValue += packageOwn.getValue();
        this.totalWeight += packageOwn.getWeight();
    }

    public void removePackage(PackageOwn packageOwn){
        this.packages.remove(packageOwn);
        this.totalValue -= packageOwn.getValue();
        this.totalWeight -= packageOwn.getWeight();
    }

    public int getTotalWeight() {
        return totalWeight;
    }

    public int getTotalValue() {
        return totalValue;
    }

    public void setTotalWeight(int totalWeight) {
        this.totalWeight = totalWeight;
    }

    @Override
    public boolean equals(Object obj) {
        for (PackageOwn packageOwn:packages) {
            boolean packageFound = false;
            for (PackageOwn packageObj:((Solution) obj).packages) {
                if (packageObj.equals(packageOwn)) {
                    packageFound = true;
                    break;
                }
            }
            if(!packageFound){
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "Solution{" +
                "packages=\n\r" + packages +
                "\n\r, totalWeight=" + totalWeight +
                "\n\r, totalValue=" + totalValue +
                '}';
    }
}