import java.util.ArrayList;

public class Launch {
    public static void main(String[] args) {
        PackageOwn pack1 = new PackageOwn(1, 4, 6);
        PackageOwn pack2 = new PackageOwn(2, 6, 9);
        PackageOwn pack3 = new PackageOwn(3, 7, 2);
        PackageOwn pack4 = new PackageOwn(4, 8, 7);
        PackageOwn pack5 = new PackageOwn(5, 1, 5);
        PackageOwn pack6 = new PackageOwn(6, 5, 3);
        PackageOwn pack7 = new PackageOwn(7, 3, 2);
        PackageOwn pack8 = new PackageOwn(8, 6, 3);
        PackageOwn pack9 = new PackageOwn(9, 5, 6);
        PackageOwn pack10 = new PackageOwn(10, 2, 4);
        PackageOwn pack11 = new PackageOwn(11, 8, 4);
        PackageOwn pack12 = new PackageOwn(12, 4, 7);
        PackageOwn pack13 = new PackageOwn(13, 5, 2);
        PackageOwn pack14 = new PackageOwn(14, 1, 1);
        PackageOwn pack15 = new PackageOwn(15, 3, 2);

        ArrayList<PackageOwn> packages = new ArrayList<>();
        packages.add(pack1);
        packages.add(pack2);
        packages.add(pack3);
        packages.add(pack4);
        packages.add(pack5);
        packages.add(pack6);
        packages.add(pack7);
        packages.add(pack8);
        packages.add(pack9);
        packages.add(pack10);
        packages.add(pack11);
        packages.add(pack12);
        packages.add(pack13);
        packages.add(pack14);
        packages.add(pack15);

        //Launch
        System.out.println("-----------------SOLUTION 1-----------------");
        Knapsack knapsack = new Knapsack(packages, 30);
        Solution solution1 = knapsack.giveASolution();
        System.out.println(solution1);

        System.out.println("-----------------NEIGHBORHOOD-----------------");
        Solution neighborhoodSolution = knapsack.giveANeighborhoodSolution(solution1);
        System.out.println(neighborhoodSolution);

        System.out.println("-----------------BEST SOLUTION-----------------");
        Solution bestSolution = knapsack.giveBestSolution(50);
        System.out.println(bestSolution);
    }
}