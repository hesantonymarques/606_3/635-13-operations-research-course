import java.util.ArrayList;

public class Knapsack {
    private final ArrayList<PackageOwn> allPackages;
    private final int capacity;


    public Knapsack(ArrayList<PackageOwn> packageOwns, int capacity) {
        this.allPackages = packageOwns;
        this.capacity = capacity;
    }

    public Solution giveASolution(){
        Solution solution = new Solution();

        int currentCapacity = 0;
        ArrayList<PackageOwn> tempPackages = (ArrayList<PackageOwn>) this.allPackages.clone();

        while(currentCapacity < capacity && !tempPackages.isEmpty()){
            //Get a random element
            int randomIndex = (int) (Math.random() * tempPackages.size());
            PackageOwn currentPackage = tempPackages.get(randomIndex);

            //If not overpassed the capacity, add the package to the solution
            if((currentCapacity + currentPackage.getWeight()) <= capacity){
                currentCapacity += currentPackage.getWeight();
                solution.addPackage(currentPackage);
            }

            //This package has been used, remove it
            tempPackages.remove(currentPackage);
        }

        return solution;
    }

    public Solution giveANeighborhoodSolution(Solution oldSolution) {
        Solution solution = new Solution(oldSolution);

        //Remove a random element from solution
        int randomIndexOldPackage = (int) (Math.random() * solution.getPackages().size());
        solution.removePackage(solution.getPackages().get(randomIndexOldPackage));

        //Take another package
        int randomIndexNewPackage;
        PackageOwn currentPackageOwn;
        ArrayList<PackageOwn> tempPackages = (ArrayList<PackageOwn>) this.allPackages.clone();
        do {
            //Get a random package in all existing packages
            randomIndexNewPackage = (int) (Math.random() * tempPackages.size());
            currentPackageOwn = tempPackages.get(randomIndexNewPackage);

            //This package shouldn't exist in the solution or should overpass the capacity
            if(solution.getPackages().contains(currentPackageOwn) || solution.getTotalWeight()+currentPackageOwn.getWeight()>capacity){
                //As it's already used randomly, remove to not find it again
                tempPackages.remove(currentPackageOwn);
            } else {
                break;
            }
        } while (!tempPackages.isEmpty());

        if(tempPackages.isEmpty()){
            return null;
        }

        //Add this package to the new solution
        solution.addPackage(currentPackageOwn);

        return solution;
    }

    public Solution giveBestSolution(int numberOfLoop) {
        Solution firstSolution = this.giveASolution();

        for (int i = 0; i < numberOfLoop; i++) {
            Solution neighborhoodSolution = this.giveANeighborhoodSolution(firstSolution);

            //System.out.println("Solution " + i + " value : " + neighborhoodSolution.getTotalValue());

            if(neighborhoodSolution.getTotalValue() > firstSolution.getTotalValue()){
                firstSolution = neighborhoodSolution;
            }
        }

        return firstSolution;
    }
}