import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Launch {
    public static void main(String[] args) {
        Job job1 = new Job(1, 5, 7);
        Job job2 = new Job(2, 3, 8);
        Job job3 = new Job(3, 8, 10);
        Job job4 = new Job(4, 7, 15);
        Job job5 = new Job(5, 2, 4);
        Job job6 = new Job(6, 5, 20);

        List<Job> jobs = new ArrayList<>();
        jobs.add(job1);
        jobs.add(job2);
        jobs.add(job3);
        jobs.add(job4);
        jobs.add(job5);
        jobs.add(job6);

        optimisePlan(jobs);
    }

    private static void optimisePlan(List<Job> jobs){
        //Sort by deadline
        jobs.sort(Comparator.comparing(Job::getDeadlineFromStart));

        System.out.println("ID\t\tStart day\t\tDuration (days)\t\tEnd day\t\tDeadline day");

        //Start day 1
        int start = 1;
        int end = -1;

        //Execute each job without any break between them in order to minimise the end of the last job (finishing asap)
        for (Job job:jobs) {
            //The end is start + duration of current job
            end = start + job.getLength();
            //The deadline is the start + unit times from start
            int deadline = start + job.getDeadlineFromStart();
            System.out.println(job.getId() + "\t\t" + start + "\t\t\t\t" + job.getLength() + "\t\t\t\t\t" + end + "\t\t\t" + deadline);

            //The next job start at the end of the previous one
            start = end;
        }

        System.out.println("You will start on day 1 and finish on day " + end);
    }
}
