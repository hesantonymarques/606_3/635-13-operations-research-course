public class Job {
    private int id;
    private int length;
    private int deadlineFromStart;

    public Job(int id, int length, int deadlineFromStart) {
        this.id = id;
        this.length = length;
        this.deadlineFromStart = deadlineFromStart;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getDeadlineFromStart() {
        return deadlineFromStart;
    }

    public void setDeadlineFromStart(int deadlineFromStart) {
        this.deadlineFromStart = deadlineFromStart;
    }

    @Override
    public String toString() {
        return "Job " + this.id + " - length " + this.length + " - deadline from start " + this.deadlineFromStart;
    }
}
