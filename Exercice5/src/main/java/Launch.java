import topology.Graph;
import topology.GraphImport;
import topology.Node;

public class Launch {
    public static void main(String[] args) {
        GraphImport graphImport = GraphImport.getInstance();
        //All modules opened
        //Graph graph = graphImport.importGraph("Exercice5/src/main/resources/tsp-graph.dot");
        //For only module Exercice5
        Graph graph = graphImport.importGraph("src/main/resources/tsp-graph.dot");

        String startS = "A"; //Start and end node

        Node start = graph.getNodes().get(startS);

        Algorithm  tspAlgo = new Algorithm(start, graph);
        tspAlgo.tspBruteForce();
        tspAlgo.displayAllSolutions();
        tspAlgo.bestSolution();
    }
}
