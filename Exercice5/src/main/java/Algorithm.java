import topology.Arc;
import topology.Graph;
import topology.Node;
import topology.impl.WeightedEdge;

import java.util.*;

public class Algorithm {
    private final String allLetters;
    private ArrayList<String> allPermutations;
    private final Node startingNode;
    private final Graph  graph;
    private final HashMap<String, Integer> allCombinations;
    private List<Map .Entry<String, Integer>> allCombinationsSorted;

    public Algorithm(Node startingNode, Graph graph) {
        this.startingNode = startingNode;
        this.graph = graph;
        this.allCombinations = new HashMap<>();

        StringBuilder stringBuilder = new StringBuilder();
        graph.getNodes().keySet().forEach(stringBuilder::append);
        this.allLetters = stringBuilder.toString();
    }

    public void tspBruteForce(){
        System.out.println(this.allLetters);

        //List of all permutions
        this.allPermutations = new ArrayList<>();
        String removeStartingNode = this.allLetters.substring(1);
        getAllPermutationsOfString(removeStartingNode.toLowerCase(Locale.ROOT), "");
        //System.out.println(this.allPermutations);
        System.out.println("Number of permutations : " + this.allPermutations.size());

        //Calculate the distance for each permutation
        for (String permutation:this.allPermutations) {
            int totalDistance = 0;
            for (int i = 0; i < permutation.toCharArray().length-1; i++) {
                Node nodeA = this.graph.getNodes().get(String.valueOf(permutation.charAt(i)));
                String nodeBString = String.valueOf(permutation.charAt(i+1));

                //Get the edge corresponding to the letters next to each other (ex. A--B), same weight A--B and B--A
                for (Arc edge:nodeA.getEdges()) {
                    if(edge.getNodeB().getName().equals(nodeBString) || edge.getNodeA().getName().equals(nodeBString)){
                        //System.out.println("Edge : " + edge);
                        int distance = ((WeightedEdge) edge).getWeight();
                        //System.out.println("Distance : " + distance);
                        totalDistance += distance;
                        break;
                    }
                }
            }

            this.allCombinations.put(permutation, totalDistance);
        }

    }

    public void displayAllSolutions(){
        //Sort the list
        this.allCombinationsSorted = new LinkedList<>(this.allCombinations.entrySet());
        this.allCombinationsSorted.sort(Map.Entry.comparingByValue());

        for (Map.Entry<String, Integer> combination: this.allCombinationsSorted) {
            System.out.println(combination.getKey() + ", distance : " + combination.getValue());
        }
    }

    public void bestSolution(){
        for (Map.Entry<String, Integer> combination: this.allCombinationsSorted) {
            //Display all solutions having the minimum distance (might be several best solutions)
            if(combination.getValue().equals(this.allCombinationsSorted.get(0).getValue())){
                System.out.println("Best path : " + combination.getKey() + ", distance : " + combination.getValue());
            } else {
                break;
            }
        }
    }

    private void getAllPermutationsOfString(String letters, String previousPermutation){
        // If string is empty
        if (letters.length() == 0) {
            //System.out.print(previousPermutation.toUpperCase(Locale.ROOT) + " ");
            String addingStartingNode = this.startingNode + previousPermutation + this.startingNode;
            this.allPermutations.add(addingStartingNode.toUpperCase(Locale.ROOT));
            return;
        }

        // Make a boolean array of size '26' which
        // stores false by default and make true
        // at the position which alphabet is being
        // used
        boolean[] alphabet = new boolean[26];

        for (int i = 0; i < letters.length(); i++) {

            // ith character of str
            char currentCharacter = letters.charAt(i);

            // Rest of the string after excluding
            // the ith character
            String ros = letters.substring(0, i) + letters.substring(i + 1);

            // If the character has not been used
            // then recursive call will take place.
            // Otherwise, there will be no recursive
            // call
            if (!alphabet[currentCharacter - 'a']){
                getAllPermutationsOfString(ros, previousPermutation + currentCharacter);
            }
            alphabet[currentCharacter - 'a'] = true;
        }
    }
}
