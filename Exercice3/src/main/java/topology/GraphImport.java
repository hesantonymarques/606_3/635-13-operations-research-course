package topology;

import topology.impl.DirectedEdge;
import topology.impl.Edge;
import topology.impl.Point;
import topology.impl.WeightedEdge;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;

public class GraphImport {
	
	private static GraphImport instance;

	//TODO	comment here
	public Graph importGraph(String fileLocation)
	{
		
		HashMap<String,Node> nodes = new HashMap<String,Node>();
		String graphName = "";
		
		String input = readFile(fileLocation);
		
		graphName = input.substring(0, input.indexOf('{'));
		String graphBody = input.substring(input.indexOf('{') + 1, input.indexOf('}'));
		
		StringTokenizer stringTokenizer = new StringTokenizer(graphBody);
		
		String token;		
		while(stringTokenizer.hasMoreTokens())
		{
			token = stringTokenizer.nextToken(";");
			
			Arc arc = null;
			
			String identifier = "";
			
			if (WeightedEdge.isType(token))
			{
				identifier = WeightedEdge.getIndentifier();
				int start = token.indexOf("weight=") + 7;
				int stop = token.indexOf("]");
				
				int weight = Integer.valueOf(token.substring(start, stop));				
				
				arc = new WeightedEdge(weight);
			}
			else if(DirectedEdge.isType(token))
			{
				identifier = DirectedEdge.getIndentifier();
				arc = new DirectedEdge();
			}
			else if(Edge.isType(token))
			{
				identifier = Edge.getIndentifier();
				arc = new Edge();
				
			}			
			
			String nodeAName = token.substring(0, token.indexOf(identifier));
			nodeAName = nodeAName.trim();
			
			
			int stop = token.contains("[") ? token.indexOf("[") : token.length();
			String nodeBName = token.substring(token.indexOf(identifier) + 2, stop);
			nodeBName = nodeBName.trim();			
			
			Point nodeA = (Point) nodes.get(nodeAName);
			Point nodeB = (Point) nodes.get(nodeBName);
			
			if(nodeA == null)
			{
				nodeA = new Point(nodeAName);
				nodes.put(nodeAName, nodeA);
			}
			
			if(nodeB == null)
			{
				nodeB = new Point(nodeBName);
				nodes.put(nodeBName, nodeB);
			}
			
			arc.setNodeA(nodeA);
			arc.setNodeB(nodeB);
			arc.setName(nodeAName + identifier + nodeBName);
						
			nodeA.addArc(arc);
			nodeB.addArc(arc);
		}	    
		
		Graph graph = new Graph(graphName, nodes);		
		
		return graph;
		
	}	
	
	private String readFile(String fileLocation)
	{
		StringBuilder stringBuilder = new StringBuilder();			

		try {
			
			BufferedReader bufferedReader = new BufferedReader(new FileReader(fileLocation));			
				 		    
		    String line = bufferedReader.readLine();
	
		    while (line != null) {
		    	
		    	stringBuilder.append(line);	        
		    	line = bufferedReader.readLine();
		    }	    
		    bufferedReader.close();
		    
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		return stringBuilder.toString();
	}
	
	//TODO	comment here
	public static GraphImport getInstance()
	{
		if(instance == null)
		{
			instance = new GraphImport();
		}
		
		return instance;
	}
	
	
}
