package topology.impl;

import topology.Arc;

//TODO	comment here
public class WeightedEdge extends Arc {

	private static String indentifier = "--";
	private int weight;
	
	public WeightedEdge(int weight) {
		
		this.weight = weight;
	}

	@Override
	public String toString()
	{
		return name;
	}
	
	//TODO	comment here
	public static boolean isType(String token)
	{
		if(token.contains(indentifier) && token.contains("weight="))
		{
			return true;
		}
		return false;
		
	}
	
	public static String getIndentifier() {
		return indentifier;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	
}
