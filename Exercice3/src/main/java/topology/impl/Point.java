package topology.impl;

import topology.Arc;
import topology.Node;

import java.util.ArrayList;
import java.util.Objects;

//TODO	comment here
public class Point implements Node{
	
	private ArrayList<Arc> edges;
	private String name;
	
	public Point(String name) {
		this.name = name;
		edges = new ArrayList<Arc>();
	}
	
	public void addArc(Arc arc)
	{
		edges.add(arc);
	}
	
	
	@Override
	public String getName() {
		return name;
	}
	@Override
	public void setName(String name) {
		this.name = name;		
	}
	@Override
	public ArrayList<Arc> getEdges() {
		return edges;
	}
	@Override
	public void setEdges(ArrayList<Arc> edges) {
		this.edges = edges;
	}
	
	public String toString()
	{		
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Point point = (Point) o;
		return Objects.equals(edges, point.edges) && Objects.equals(name, point.name);
	}
}
