package topology;

//TODO	comment here
public abstract class Arc {

	protected String name;
	protected Node nodeA;
	protected Node nodeB;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Node getNodeA() {
		return nodeA;
	}
	public void setNodeA(Node nodeA) {
		this.nodeA = nodeA;
	}
	public Node getNodeB() {
		return nodeB;
	}
	public void setNodeB(Node nodeB) {
		this.nodeB = nodeB;
	}

	//TODO	comment here
	@Override
	public abstract String toString();
}
