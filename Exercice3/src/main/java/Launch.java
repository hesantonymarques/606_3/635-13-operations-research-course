import topology.Graph;
import topology.GraphImport;
import topology.Node;

public class Launch {
    public static void main(String[] args) {
        GraphImport graphImport = GraphImport.getInstance();
        Graph graph = graphImport.importGraph("src/main/resources/graph3.dot");

        String startS = "1";
        String goalS = "10";

        Node start = graph.getNodes().get(startS);
        Node goal = graph.getNodes().get(goalS);

        Algorithm bfsAlgorithm = new BFS();
        Algorithm dfsAlgorithm = new DFS();

        System.out.println("-------------------- BFS --------------------");
        System.out.println("Path from " + startS + " to " + goalS + " is : " + bfsAlgorithm.resolve(goal, start, graph));

        System.out.println("-------------------- DFS --------------------");
        System.out.println("Path from " + startS + " to " + goalS + " is : " + dfsAlgorithm.resolve(goal, start, graph));
    }
}
