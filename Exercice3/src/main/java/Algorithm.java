import tools.Tools;
import topology.Arc;
import topology.Graph;
import topology.Node;

import java.util.ArrayList;
import java.util.List;

public abstract class Algorithm {
    public List<Node> resolve(Node goal, Node start, Graph graph){
        Node currentNode;
        List<Node> visitedNodes = new ArrayList<>();
        List<Node> toVisit = new ArrayList<>(); //used as a Queue or Stack
        boolean found = false;

        //Add starting node to visit
        toVisit.add(start);

        while (!found && !toVisit.isEmpty()){
            System.out.println("toVisit : " + toVisit + " - visitedNodes : " + visitedNodes);

            currentNode = toVisit.remove(0);
            visitedNodes.add(currentNode);
            List<Node> neighbors = graph.getNextNeighbors(currentNode);
            if(goal.equals(currentNode)){
                found = true;
            } else{
                addNeighbors(toVisit, visitedNodes, neighbors); //ONLY DIFFERENCE BETWEEN BFS AND DFS
            }
        }

        System.out.println("toVisit : " + toVisit + " - visitedNodes : " + visitedNodes);

        if(found){
            //Okay, we terminate the loop
            //nodeVisted = [1,2,7,8,3,6,9,12,4,5,10]
            //Reverse arrayList
            List<Node> reversedVisitedNodes = Tools.reverseList(visitedNodes);
            //System.out.println("Reverse visited nodes : " + reversedVisitedNodes);
            for (int i = 0; i < reversedVisitedNodes.size()-1; i++) {
                boolean isLinked = false;
                //System.out.println("Edges for " + reversedVisitedNodes.get(i) + " : " + reversedVisitedNodes.get(i).getEdges());
                for (Arc e:reversedVisitedNodes.get(i).getEdges()){
                    //System.out.println("Checking " + reversedVisitedNodes.get(i) + " linked to " + reversedVisitedNodes.get(i+1));
                    if(e.getNodeA().equals(reversedVisitedNodes.get(i+1))){
                        //System.out.println("IS LINKED");
                        isLinked = true;
                        break;
                    }
                }

                if(!isLinked){
                    //System.out.println("This node will be removed : " + reversedVisitedNodes.get(i+1));
                    reversedVisitedNodes.remove(reversedVisitedNodes.get(i+1));
                    i--;
                }
            }

            //System.out.println("Path : " + Tools.reverseList(reversedVisitedNodes));

            return Tools.reverseList(reversedVisitedNodes);
        }

        return new ArrayList<>();
    }

    public void addNeighbors(List<Node> toVisit, List<Node> visitedNodes, List<Node> neighbors) {}
}
