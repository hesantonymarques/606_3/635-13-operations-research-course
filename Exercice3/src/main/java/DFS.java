import topology.Node;

import java.util.ArrayList;
import java.util.List;

public class DFS extends Algorithm{

    @Override
    public void addNeighbors(List<Node> toVisit, List<Node> visitedNodes, List<Node> neighbors){
        List<Node> temp = new ArrayList<>();

        for (Node neighbor:neighbors) {
            if(!toVisit.contains(neighbor) && !visitedNodes.contains(neighbor)){
                temp.add(neighbor);
            }
        }

        toVisit.addAll(0, temp); //Used as a Stack
    }
}
