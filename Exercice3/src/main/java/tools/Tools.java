package tools;

import topology.Node;

import java.util.ArrayList;
import java.util.List;

public class Tools {
    public static List<Node> reverseList(List<Node> list){
        List<Node> reversedList = new ArrayList<>();

        for (int i = list.size() - 1; i >= 0; i--) {
            reversedList.add(list.get(i));
        }

        return reversedList;
    }
}
