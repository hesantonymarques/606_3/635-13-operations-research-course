package topology;

import org.junit.Before;
import org.junit.Test;
import topology.impl.WeightedEdge;

import static org.junit.Assert.assertTrue;


//TODO	comment here
public class WeightedEdgeGraphTest {
	
	
	private Graph graph;
	
	//TODO	comment here
	@Before
	public void setup()
	{
		GraphImport graphImport = GraphImport.getInstance();		
		graph = graphImport.importGraph("src/test/resources/weightedGraph1.dot");
	}
	

	@Test
	public void testGraphImportNull() {
				
		assertTrue(graph != null);
	}
	
	@Test
	public void testGraphImportEmpty() {
		
		assertTrue(graph.getName() != null && !graph.getNodes().isEmpty());
	}
	
	@Test
	public void testGraphImportLastNode() {
		
		assertTrue(graph.getNodes().get("3") != null);
	}
	
	
	@Test
	public void testGraphImportNumVertices() {
		
		assertTrue(graph.getNodes().size() == 8);
	}
	
	@Test
	public void testGraphImportIsWeighted() {
		
		WeightedEdge edge = (WeightedEdge) graph.getNodes().get("3").getEdges().get(0);
		assertTrue(edge.getWeight() > 0);
	}


}
