package topology;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertTrue;

//TODO	comment here
public class EdgeGraphTest {
	
	
	private Graph graph;
	
	//TODO	comment here
	@Before
	public void setup()
	{
		GraphImport graphImport = GraphImport.getInstance();		
		graph = graphImport.importGraph("src/test/resources/graph1.dot");
		for (Map.Entry<String, Node> n : graph.getNodes().entrySet()) {
			//System.out.print(n.getKey() + " : ");
			//System.out.println(n.getValue().getEdges());
			System.out.print("NEIGHBORS for " + n.getKey());
			System.out.println(graph.getNextNeighbors(n.getValue()));
		}
	}
	

	@Test
	public void testGraphImportNull() {
				
		assertTrue(graph != null);
	}
	
	@Test
	public void testGraphImportEmpty() {
		
		assertTrue(graph.getName() != null && !graph.getNodes().isEmpty());
	}
	
	@Test
	public void testGraphImportLastNode() {
		
		assertTrue(graph.getNodes().get("3") != null);
	}
	
	
	@Test
	public void testGraphImportNumVertices() {
		
		assertTrue(graph.getNodes().size() == 8);
	}


}
