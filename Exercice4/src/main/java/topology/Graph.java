package topology;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {
	
	private String name;
	private HashMap<String,Node> nodes;
	
	public Graph(String name, HashMap<String,Node> nodes) {
		this.name = name;
		this.nodes = nodes;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public HashMap<String,Node> getNodes() {
		return nodes;
	}
	public void setNodes(HashMap<String,Node> nodes) {
		this.nodes = nodes;
	}

	public List<Node> getNextNeighbors(Node node){
		List<Node> neighbors = new ArrayList<>();
		for (Map.Entry<String, Node> n : nodes.entrySet()) {
			if(n.getValue().equals(node)){
				for (Arc e:n.getValue().getEdges()){
					if(e.nodeA.equals(n.getValue())){
						neighbors.add(e.nodeB); //No need to add visited and current node
					}
				}
			}
		}
		return neighbors;
	}
}
