package topology;

import java.util.ArrayList;

//TODO	comment here
public interface Node {
	
	public ArrayList<Arc> getEdges();
	public String getName();	
	
	public void setName(String name);
	public void setEdges(ArrayList<Arc> edges);
	
	//TODO	comment here
	@Override
	public String toString();

}
