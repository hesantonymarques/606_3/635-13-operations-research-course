import topology.Arc;
import topology.Graph;
import topology.Node;
import topology.impl.WeightedEdge;

import java.util.*;

public class Algorithm {
    public static void dijkstras(Node start, Graph graph) {
        HashMap<String, Integer> distances = new HashMap<>();
        HashMap<String, String> predecessors = new HashMap<>();

        Node currentNode;
        List<Node> visitedNodes = new ArrayList<>(); //We don't really care about that because we don't check if I already visited that node, just to console log it
        List<Node> toVisit = new ArrayList<>();

        //Add starting node to visit
        toVisit.add(start);

        //Distance for starting node is 0
        distances.put(start.getName(), 0);

        //Predecessor for starting node is null
        predecessors.put(start.getName(), null);

        while(!toVisit.isEmpty()){
            //System.out.println("toVisit : " + toVisit + " - visitedNodes : " + visitedNodes);

            currentNode = toVisit.remove(0);
            visitedNodes.add(currentNode);

            //Set distance and predecessor for edges linked to current node
            for (Arc edge:currentNode.getEdges()) {
                String nodeBName = edge.getNodeB().getName();

                //Only need edges from A (current node) to B (neighbor node)
                if(!nodeBName.equals(currentNode.getName())){
                    int distance = distances.get(currentNode.getName()) + ((WeightedEdge) edge).getWeight();

                    //Check not found error
                    int oldDistance = -1;
                    if(distances.get(nodeBName) != null){
                        oldDistance = distances.get(nodeBName);
                    }

                    //System.out.println("Old distance for " + nodeBName + " : " + oldDistance);

                    //If no old distance or new distance less to old distance, make the change
                    if(oldDistance == -1 || distance < oldDistance){
                        distances.put(nodeBName, distance);
                        predecessors.put(nodeBName, currentNode.getName());

                        //System.out.println("Edge for " + currentNode.getName() + " : " + edge);
                        //System.out.println("Distance : " + distances.get(nodeBName));
                        //System.out.println("Predecessor : " + predecessors.get(nodeBName));

                        toVisit.add(edge.getNodeB());
                    }
                }
            }

            //Order the list based on the distance weight
            toVisit = orderListGoNext(toVisit, distances);
        }

        //System.out.println("toVisit : " + toVisit + " - visitedNodes : " + visitedNodes);

        System.out.println("RESULT :");
        System.out.println("v\td\tpred");
        System.out.println("------------");
        for (Map.Entry<String, Node> n:graph.getNodes().entrySet()) {
            System.out.println(n.getKey() + "\t" + distances.get(n.getKey()) + "\t" + predecessors.get(n.getKey()));
        }
    }

    public static List<Node> orderListGoNext(List<Node> toVisit, HashMap<String, Integer> distances){
        //System.out.println(distances);
        HashMap<String, Integer> sortedDistances = sortByDistance(distances);
        //System.out.println(sortedDistances);

        List<Node> newList = new ArrayList<>();

        //Reorder the toVisit list based on the distance
        for (Map.Entry<String, Integer> distance:sortedDistances.entrySet()) {
            Node interestingNode = null;
            for (Node node:toVisit) {
                if(node.getName().equals(distance.getKey())){
                    interestingNode = node;
                    break;
                }
            }

            if(interestingNode != null){
                newList.add(interestingNode);
            }
        }

        return newList;
    }

    public static HashMap<String, Integer> sortByDistance(HashMap<String, Integer> distances){
        List<Map .Entry<String, Integer>> list = new LinkedList<>(distances.entrySet());

        //Sort the list
        list.sort(Map.Entry.comparingByValue());

        //Return it into Hashmap
        HashMap<String, Integer> temp = new LinkedHashMap<>();

        for (Map.Entry<String, Integer> element:list) {
            temp.put(element.getKey(), element.getValue());
        }

        return temp;
    }
}
