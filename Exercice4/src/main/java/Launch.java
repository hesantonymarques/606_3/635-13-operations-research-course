import topology.Graph;
import topology.GraphImport;
import topology.Node;

public class Launch {
    public static void main(String[] args) {
        GraphImport graphImport = GraphImport.getInstance();
        //All modules opened
        //Graph graph = graphImport.importGraph("Exercice4/src/main/resources/searchgraph-weighted.dot");
        //For only module Exercice4
        Graph graph = graphImport.importGraph("src/main/resources/searchgraph-weighted.dot");

        String startS = "A";

        Node start = graph.getNodes().get(startS);

        Algorithm.dijkstras(start, graph);
    }
}
